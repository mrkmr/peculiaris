# Read a file's entire contents.
function getfile(file) {
    save_rs = RS
    RS = "^$"
    getline ret < file
    close(file)
    RS = save_rs

    return ret
}

function style_block() {
    return \
    "\n"                                   \
    "<style>\n"                            \
    "body {\n"                             \
    "    margin: 40px auto;\n"             \
    "    max-width: 650px;\n"              \
    "}\n"                                  \
    "\n"                                   \
    "code {\n"                             \
    "    background: lightgray;\n"         \
    "    padding: 2px;\n"                  \
    "}\n"                                  \
    "\n"                                   \
    "pre {\n"                              \
    "    background: lightgray;\n"         \
    "    padding: 10px;\n"                 \
    "}\n"                                  \
    "\n"                                   \
    "table#postlist {\n"                   \
    "    border: 3px solid;\n"             \
    "    table-layout: fixed;\n"           \
    "    text-align: left;\n"              \
    "}\n"                                  \
    "\n"                                   \
    "table, th, td {\n"                    \
    "   border: 1px solid;\n"              \
    "   padding: 3px;\n"                   \
    "}\n"                                  \
    "\n"                                   \
    "th {\n"                               \
    "    border: 2px solid;\n"             \
    "}\n"                                  \
    "\n"                                   \
    ".post_content {\n"                    \
    "    border: 2px solid;\n"             \
    "    padding: 10px;\n"                 \
    "}\n"                                  \
    "</style>\n"                           \
    "\n"                                   \
}

function output_body_stage_one(title) {
    return                                                        \
    "<!DOCTYPE html>\n"                                           \
    "<html lang=\"en\">\n"                                        \
    "\n"                                                          \
    "<!-- Header Content -->\n"                                   \
    "<head>\n"                                                    \
    "    <title>" title "</title>\n"                              \
    "    <meta charset=\"UTF-8\">\n"                              \
    "    <meta name=\"viewport\"\n"                               \
    "          content=\"width=device-width,initial-scale=1\">\n" \
    "    <meta name=\"Description\"\n"                            \
    "          content=\"mrk's website of unix/prog stuff\">\n"   \
    "\n"                                                          \
    style_block()                                                 \
    "\n"                                                          \
    "</head>\n"                                                   \
    "\n"                                                          \
    "<!-- Body Content -->\n"                                     \
    "<body>\n"                                                    \
    "\n"                                                          \
    "<h1>Welcome to mrk's website!</h1>\n"                        \
    "\n"
}

function output_body_stage_two() {
    return      \
    "\n"        \
    "</body>\n" \
    "\n"        \
    "</html>\n"
}

# Run a command and error out if it fails.
function system_cmd(cmd) {
    print "Running command: " cmd
    if (system(cmd) != 0) {
        printf "Command failed: " cmd > /dev/stderr
        exit 1
    }
}

# Remove current output dirs and recreate needed directories.
# Define out directory.
BEGIN {
    # Useful variables.
    OUTDIR = "public"
    system_cmd("rm -rf " OUTDIR)
    system_cmd("mkdir -p " OUTDIR "/posts")
}

# Start the index.html file
BEGIN {
    index_content = \
    output_body_stage_one("mrk's website") \
    "<header>\n"                           \
    "<h2>Posts</h2>\n"                     \
    "</header>\n"                          \
    "<table id=\"postlist\">\n"            \
    "<tr>\n"                               \
    "    <th>Title</th>\n"                 \
    "    <th>Date</th>\n"                  \
    "    <th>Updated</th>\n"               \
    "</tr>\n"                              \
    "\n"
}

# Posts are separated by newline.
BEGIN { FS = "\n"; RS = "" }
# Skip comment lines.
/^#/ {next}
{
    # What does each line mean?
    title   = $1 # The title of the post
    original= $2 # Date when originally made.
    file    = $3 # HTML file location to make post content from.
    updated = $4 # Possibly empty. When the post was updated.
    if (updated == "") {
        updated = ""
    }

    # Output html with custom title.
    # Add a back button to go to index.
    output = \
    output_body_stage_one("mrk - " title)                 \
    "<header>\n"                                          \
    "<h2>" title "</h2>\n"                                \
    "</header>\n"                                         \
    "\n"                                                  \
    "<nav><a href=\"../index.html\">Previous</a></nav>\n" \
    "\n"                                                  \
    "<div class=\"post_content\">\n"                      \
    getfile(file)                                         \
    "</div>\n"                                            \
    output_body_stage_two()

    # Write out the file.
    outfile = OUTDIR "/" file
    print "Writing " outfile
    printf "%s", output > outfile

    # Add post to index's table.
    "    <a href=" file ">"
    index_content = index_content     \
    "<tr>\n"                          \
    "    <td>\n"                      \
    "        <a href=\"" file "\">\n" \
    "            " title "\n"         \
    "        </a>\n"                  \
    "    </td>\n"                     \
    "    <td>" original "</td>\n"     \
    "    <td>" updated  "</td>\n"     \
    "</tr>\n"
}

# Finish index.html content and write it out.
END {
    # End the table.
    index_content = index_content \
    "</table>\n"                  \
    output_body_stage_two()

    print "Writing public/index.html"
    printf "%s", index_content > OUTDIR "/index.html"
}
